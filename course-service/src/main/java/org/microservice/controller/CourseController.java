package org.microservice.controller;

import lombok.AllArgsConstructor;
import org.microservice.dto.CourseDto;
import org.microservice.model.request.CourseRequest;
import org.microservice.response.APIResponse;
import org.microservice.service.CourseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/courses")
@AllArgsConstructor
public class CourseController {
    private final CourseService courseService;

    @PostMapping("")
    public ResponseEntity<APIResponse<CourseDto>> addNewCourse(@RequestBody CourseRequest courseRequest){
        return ResponseEntity.ok().body(courseService.addNewCourse(courseRequest));
    }

    @GetMapping("")
    public ResponseEntity<APIResponse<List<CourseDto>>> getAllCourses(){
        return ResponseEntity.ok().body(courseService.getAllCourses());
    }

    @GetMapping("{id}")
    public ResponseEntity<CourseDto> getCourseById(@PathVariable Long id){
        return ResponseEntity.ok().body(courseService.getCourseById(id));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<APIResponse<CourseDto>> deleteCourseById(@PathVariable Long id){
        return ResponseEntity.ok().body(courseService.deleteCourseById(id));
    }

    @PutMapping("{id}")
    public ResponseEntity<APIResponse<CourseDto>> updateCourse(@PathVariable Long id, @RequestBody CourseRequest courseRequest){
        return ResponseEntity.ok().body(courseService.updateCourse(id, courseRequest));
    }
}
