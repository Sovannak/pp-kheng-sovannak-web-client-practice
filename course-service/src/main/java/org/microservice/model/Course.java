package org.microservice.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.microservice.dto.CourseDto;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "courses")
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String courseName;
    private String courseCode;
    private String description;
    private String instructor;

    public CourseDto toDto(){
        return new CourseDto(this.id, this.courseName, this.courseCode, this.description, this.instructor);
    }
}
