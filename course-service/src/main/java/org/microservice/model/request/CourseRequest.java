package org.microservice.model.request;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.microservice.model.Course;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CourseRequest {
    private String courseName;
    private String courseCode;
    private String description;
    private String instructor;

    public Course toEntity(){
        return new Course(null, this.courseName, this.courseCode, this.description, this.instructor);
    }

    public Course toEntity(Long id){
        return new Course(id, this.courseName, this.courseCode, this.description, this.instructor);
    }
}
