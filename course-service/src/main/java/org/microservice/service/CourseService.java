package org.microservice.service;

import org.microservice.dto.CourseDto;
import org.microservice.model.request.CourseRequest;
import org.microservice.response.APIResponse;

import java.util.List;
import java.util.Optional;

public interface CourseService {
    APIResponse<CourseDto> addNewCourse(CourseRequest courseRequest);

    APIResponse<List<CourseDto>> getAllCourses();

    CourseDto getCourseById(Long id);

    APIResponse<CourseDto> deleteCourseById(Long id);

    APIResponse<CourseDto> updateCourse(Long id, CourseRequest courseRequest);
}
