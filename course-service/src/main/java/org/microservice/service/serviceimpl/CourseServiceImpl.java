package org.microservice.service.serviceimpl;

import lombok.AllArgsConstructor;
import org.microservice.dto.CourseDto;
import org.microservice.exception.NotFoundException;
import org.microservice.model.Course;
import org.microservice.model.request.CourseRequest;
import org.microservice.repository.CourseRepository;
import org.microservice.response.APIResponse;
import org.microservice.service.CourseService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CourseServiceImpl implements CourseService {

    private final CourseRepository courseRepository;

    @Override
    public APIResponse<CourseDto> addNewCourse(CourseRequest courseRequest) {
        return APIResponse.<CourseDto>builder()
                .message("successful add new course")
                .httpStatus(HttpStatus.CREATED)
                .payload(courseRepository.save(courseRequest.toEntity()).toDto())
                .build();
    }

    @Override
    public APIResponse<List<CourseDto>> getAllCourses() {
        return APIResponse.<List<CourseDto>>builder()
                .message("successful get all courses")
                .httpStatus(HttpStatus.OK)
                .payload(courseRepository.findAll().stream().map(Course::toDto).toList())
                .build();
    }

    @Override
    public CourseDto getCourseById(Long id) {
        return courseRepository.findById(id).orElseThrow(() -> new NotFoundException("This course not found")).toDto();
    }

    @Override
    public APIResponse<CourseDto> deleteCourseById(Long id) {
        Course course = courseRepository.findById(id).orElseThrow(() -> new NotFoundException("This course not found"));
        courseRepository.deleteById(course.getId());
        return APIResponse.<CourseDto>builder()
                .message("successful delete course")
                .httpStatus(HttpStatus.OK)
                .payload(null)
                .build();
    }

    @Override
    public APIResponse<CourseDto> updateCourse(Long id, CourseRequest courseRequest) {
        Course course = courseRepository.findById(id).orElseThrow(() -> new NotFoundException("This course not found"));
        return APIResponse.<CourseDto>builder()
                .message("successful delete course")
                .httpStatus(HttpStatus.OK)
                .payload(courseRepository.save(courseRequest.toEntity(course.getId())).toDto())
                .build();
    }
}
