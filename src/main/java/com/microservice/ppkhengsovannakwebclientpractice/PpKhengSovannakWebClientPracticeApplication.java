package com.microservice.ppkhengsovannakwebclientpractice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PpKhengSovannakWebClientPracticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(PpKhengSovannakWebClientPracticeApplication.class, args);
	}

}
