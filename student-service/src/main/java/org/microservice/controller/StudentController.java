package org.microservice.controller;

import lombok.AllArgsConstructor;
import org.microservice.model.dto.StudentDto;
import org.microservice.model.request.StudentRequest;
import org.microservice.response.APIResponse;
import org.microservice.service.StudentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/students")
@AllArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @PostMapping("")
    public ResponseEntity<APIResponse<StudentDto>> addNewStudent(@RequestBody StudentRequest studentRequest){
        return ResponseEntity.ok().body(studentService.addNewStudent(studentRequest));
    }

    @GetMapping("")
    public ResponseEntity<APIResponse<List<StudentDto>>> getAllStudents(){
        return ResponseEntity.ok().body(studentService.getAllStudents());
    }

    @GetMapping("{id}")
    public ResponseEntity<StudentDto> getStudentById(@PathVariable Long id){
        return ResponseEntity.ok().body(studentService.getStudentById(id));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<APIResponse<StudentDto>> deleteStudentById(@PathVariable Long id){
        return ResponseEntity.ok().body(studentService.deleteStudentById(id));
    }

    @PutMapping("{id}")
    public ResponseEntity<APIResponse<StudentDto>> updateStudent(@PathVariable Long id, @RequestBody StudentRequest studentRequest){
        return ResponseEntity.ok().body(studentService.updateStudent(id, studentRequest));
    }
}
