package org.microservice.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.microservice.dto.CourseDto;
import org.microservice.model.dto.StudentDto;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "students")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private LocalDateTime birthDate;
    private Long courseId;

    public StudentDto toDto(CourseDto courseDto){
        return new StudentDto(this.id, this.firstName, this.lastName, this.email, this.birthDate, courseDto);
    }
}
