package org.microservice.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.microservice.model.Student;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentRequest {
    private String firstName;
    private String lastName;
    private String email;
    private LocalDateTime birthDate;
    private Long courseId;

    public Student toEntity(){
        return new Student(null, this.firstName, this.lastName, this.email, this.birthDate, this.courseId);
    }

    public Student toEntity(Long id){
        return new Student(id, this.firstName, this.lastName, this.email, this.birthDate, this.courseId);
    }
}
