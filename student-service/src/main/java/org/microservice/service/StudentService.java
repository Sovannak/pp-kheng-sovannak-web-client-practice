package org.microservice.service;

import org.microservice.model.dto.StudentDto;
import org.microservice.model.request.StudentRequest;
import org.microservice.response.APIResponse;

import java.util.List;

public interface StudentService {
    APIResponse<StudentDto> addNewStudent(StudentRequest studentRequest);

    APIResponse<List<StudentDto>> getAllStudents();

    StudentDto getStudentById(Long id);

    APIResponse<StudentDto> deleteStudentById(Long id);

    APIResponse<StudentDto> updateStudent(Long id, StudentRequest studentRequest);
}
