package org.microservice.service.serviceimpl;

import lombok.AllArgsConstructor;
import org.microservice.dto.CourseDto;
import org.microservice.exception.NotFoundException;
import org.microservice.model.Student;
import org.microservice.model.dto.StudentDto;
import org.microservice.model.request.StudentRequest;
import org.microservice.repository.StudentRepository;
import org.microservice.response.APIResponse;
import org.microservice.service.StudentService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

@Service
@AllArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final WebClient webClient;

    @Override
    public APIResponse<StudentDto> addNewStudent(StudentRequest studentRequest) {
        CourseDto courseDto = webClient.get()
                .uri("/{id}", studentRequest.getCourseId())
                .retrieve()
                .bodyToMono(CourseDto.class)
                .share().block();
        return APIResponse.<StudentDto>builder()
                .message("successful add new student")
                .httpStatus(HttpStatus.CREATED)
                .payload(studentRepository.save(studentRequest.toEntity()).toDto(courseDto))
                .build();
    }

    @Override
    public APIResponse<List<StudentDto>> getAllStudents() {
        return APIResponse.<List<StudentDto>>builder()
                .message("successful get all students")
                .httpStatus(HttpStatus.OK)
                .payload((studentRepository.findAll().stream().map(student -> student.toDto(webClient.get()
                        .uri("/{id}", student.getCourseId())
                        .retrieve()
                        .bodyToMono(CourseDto.class)
                        .share().block())).toList()))
                .build();
    }

    @Override
    public StudentDto getStudentById(Long id) {
        return studentRepository.findById(id).map(student -> student.toDto(webClient.get()
                .uri("/{id}", student.getCourseId())
                .retrieve()
                .bodyToMono(CourseDto.class)
                .share().block())).orElseThrow(() -> new NotFoundException("Student not found"));
    }

    @Override
    public APIResponse<StudentDto> deleteStudentById(Long id) {
        Student student = studentRepository.findById(id).orElseThrow(() -> new NotFoundException("Student not found"));
        studentRepository.deleteById(student.getId());
        return APIResponse.<StudentDto>builder()
                .message("successful delete student by id")
                .httpStatus(HttpStatus.OK)
                .payload(null)
                .build();
    }

    @Override
    public APIResponse<StudentDto> updateStudent(Long id, StudentRequest studentRequest) {
        Student student = studentRepository.findById(id).orElseThrow(() -> new NotFoundException("Student not found"));
        return  APIResponse.<StudentDto>builder()
                .message("successful delete student by id")
                .httpStatus(HttpStatus.OK)
                .payload(studentRepository.save(studentRequest.toEntity(student.getId())).toDto(webClient.get()
                        .uri("/{id}", student.getCourseId())
                        .retrieve()
                        .bodyToMono(CourseDto.class)
                        .share().block()))
                .build();
    }
}
